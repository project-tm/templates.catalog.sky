<?php

use Bitrix\Main\Loader,
    Bitrix\Main\Page\Asset,
    Bitrix\Main\Application,
    Project\Core\Model\FavoritesTable;

class ProjectFavorites extends CBitrixComponent {

    private function startWrapperTemplate() {
        Asset::getInstance()->addJs($this->arResult['SCRIPT']);
        $jsParams = array(
            'AJAX' => $this->arResult['AJAX'],
            'TYPE' => $this->arParams['TYPE'],
            'SELECT' => $this->arParams['SELECT'],
            'ELEMENT_ID' => $this->arParams['ELEMENT_ID'],
            'INFAVORITES' => $this->arResult['INFAVORITES']
        );
        ?>
        <script>
            var <?= $this->arResult['JS_OBJECT'] ?> = new jsProjectFavorites(<?= CUtil::PhpToJSObject($jsParams, false, true) ?>);
        </script>
        <?
    }

    public function executeComponent() {
        if (Loader::includeModule('project.core') and CUser::IsAuthorized()) {
            $this->arResult['IS_AJAX'] = isset($this->arParams['IS_AJAX']) and $this->arParams['IS_AJAX'] == 'Y';

            $userId = cUser::GetID();
            $request = Application::getInstance()->getContext()->getRequest();
            if ($this->arResult['IS_AJAX']) {
                if ($this->arParams['ELEMENT_ID']) {

                    $rsData = FavoritesTable::getList(array(
                                'select' => array('ID'),
                                'filter' => array(
                                    'TYPE' => $this->arParams['TYPE'],
                                    'USER_ID' => $userId,
                                    'ELEMENT_ID' => $this->arParams['ELEMENT_ID']
                                ),
                    ));
                    $rsData = new CDBResult($rsData);
                    $arItem = $rsData->Fetch();
                    if ((int) $request->get('ADD')) {
                        if (!$arItem) {
                            if (!FavoritesTable::add(array(
                                        'TYPE' => $this->arParams['TYPE'],
                                        'USER_ID' => $userId,
                                        'ELEMENT_ID' => $this->arParams['ELEMENT_ID']
                                    ))) {
                                throw new Exception('ERR');
                            }
                        }
                    } else {
                        if ($arItem) {
                            FavoritesTable::Delete($arItem['ID']);
                        }
                    }
                    return true;
                }
                return false;
            }

            if ($this->arParams['ELEMENTS']) {
                $rsData = FavoritesTable::getList(array(
                            'select' => array('ELEMENT_ID'),
                            'filter' => array(
                                'TYPE' => $this->arParams['TYPE'],
                                'USER_ID' => $userId,
                                'ELEMENT_ID' => $this->arParams['ELEMENTS']
                            ),
                ));
                $rsData = new CDBResult($rsData);
                while ($arItem = $rsData->Fetch()) {
                    $this->arResult['INFAVORITES'][] = $arItem['ELEMENT_ID'];
                }
            }

            $this->arResult['AJAX'] = $this->GetPath() . '/ajax.php';
            $this->arResult['SCRIPT'] = $this->GetPath() . '/script.js';
            $this->arResult['TEMPLATE_NAME'] = $this->GetTemplateName();
            $key = sha1($this->arResult['TEMPLATE_NAME'] . serialize($this->arParams));

            $this->arResult['JS_OBJECT'] = 'jsAjaxFavorites' . $key;
            $this->startWrapperTemplate();
        }
    }

}
