<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<? if(!defined('TEMPLATES_IS_POPUP')) { ?>
<? if (!defined('TEMPLATES_IS_MAIN')and ! defined('TEMPLATES_IS_CATALOG') and !defined('TEMPLATES_IS_PERSONAL')) { ?>
    </div>
    </div>
    </div>
    </div>
<? } ?>
</div>
</div>
<? $APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR . "/include/footer/popup.php")); ?>
<? $APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR . "/include/footer/vk.php")); ?>
<div class="footer_bg_color">
    <div id="footer" itemscope="" itemtype="http://schema.org/LocalBusiness">
        <div class="container">
            <div class="four columns">
                <div class="h3">Информация</div>
                <a class="open-close" href="#"></a>
                <div class="wrapper">
                    <?
                    $APPLICATION->IncludeComponent("bitrix:menu", "menu.footer", Array(
                        "ROOT_MENU_TYPE" => "bottom", // Тип меню для первого уровня
                        "MAX_LEVEL" => "1", // Уровень вложенности меню
                        "CHILD_MENU_TYPE" => "", // Тип меню для остальных уровней
                        "USE_EXT" => "N", // Подключать файлы с именами вида .тип_меню.menu_ext.php
                        "DELAY" => "N", // Откладывать выполнение шаблона меню
                        "ALLOW_MULTI_SELECT" => "N", // Разрешить несколько активных пунктов одновременно
                        "MENU_CACHE_TYPE" => "N", // Тип кеширования
                        "MENU_CACHE_TIME" => "3600", // Время кеширования (сек.)
                        "MENU_CACHE_USE_GROUPS" => "Y", // Учитывать права доступа
                        "MENU_CACHE_GET_VARS" => "", // Значимые переменные запроса
                        "COMPONENT_TEMPLATE" => "menu.footer"
                            ), false
                    );
                    ?>
                </div>
            </div>
            <div class="four columns">
                <div class="h3">Варианты оплаты</div>
                <a class="open-close" href="#"></a>
                <div class="wrapper">
                    <div id="payment_services">
                        <? $APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR . "/include/footer/pay.php")); ?>
                    </div>
                </div>
            </div>
            <div class="four columns">
                <div class="h3">Контакты</div>
                <a class="open-close" href="#"></a>
                <div class="wrapper">
                    <? $APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR . "/include/footer/contacts.php")); ?>
                </div>
            </div>

            <div class="four columns">
                <div class="h3">Обратная связь</div>
                <a class="open-close" href="#"></a>
                <div class="wrapper">
                    <? $APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR . "/include/footer/feedback.php")); ?>
                </div>
            </div>

            <div id="copyright">
                <div class="container">
                    <div class="copyright">
                        <? $APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR . "/include/footer/copyright.php")); ?>
                    </div>
                    <div itemprop="address" itemscope="" itemtype="http://schema.org/PostalAddress">
                        <div>
                            <? $APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR . "/include/footer/adress.php")); ?>
                        </div>
                    </div>
                </div>
                <div class="team">
                    <? $APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR . "/include/footer/team.php")); ?>
                </div>
                <div class="container"></div>
            </div>
            <div class="clear"></div>
        </div>
    </div>

    <div class="btn-top" id="back-top" title="Наверх">
        <img class="img-top" src="<?= SITE_TEMPLATE_PATH . '/assets/images/scroll.png'; ?>">
    </div>
    <div class="clear"></div>
</div>
<?/*форма обратного звонка*/?>
<div class="callbakc_overlay">
<div class="callbakc_wrap">
<div class="callback_close_wrap"><a href="#" class="callback_close">+</a></div>
<div class="callbakc_form">
<p class="callback_head">Обратный звонок</p>
	<form method="post" action="#">
		<label>Ваше имя<span>*</span>:</label>
		<input type="text" name="name" value="" />
		<label>Номер телефона<span>*</span>:</label>
		<input type="text" name="phone" value="" />
		<label>Удобное время для звонка</label>
		<div class="time_wrap">
			<span>с</span>
			<select name="time_before">
				<option>9:00</option>
				<option>9:30</option>
				<option>10:00</option>
				<option>10:30</option>
				<option>11:00</option>
				<option>11:30</option>
				<option>12:00</option>
				<option>12:30</option>
				<option>13:00</option>
				<option>13:30</option>
				<option>14:00</option>
				<option>14:30</option>
				<option>15:00</option>
				<option>15:30</option>
				<option>16:00</option>
				<option>16:30</option>
				<option>17:00</option>
				<option>17:30</option>
				<option>18:00</option>
				<option>18:30</option>
				<option>19:00</option>
				<option>19:30</option>
			</select>
			<span>по</span>
			<select name="time_after">
				<option>Не выбрано</option>
				<option>9:00</option>
				<option>9:30</option>
				<option>10:00</option>
				<option>10:30</option>
				<option>11:00</option>
				<option>11:30</option>
				<option>12:00</option>
				<option>12:30</option>
				<option>13:00</option>
				<option>13:30</option>
				<option>14:00</option>
				<option>14:30</option>
				<option>15:00</option>
				<option>15:30</option>
				<option>16:00</option>
				<option>16:30</option>
				<option>17:00</option>
				<option>17:30</option>
				<option>18:00</option>
				<option>18:30</option>
				<option>19:00</option>
				<option>19:30</option>
			</select>
		</div>
		 <div class="errors"></div>
		<div style="clear:both;"></div>
		<input type="submit" id="callback_sbmt_js" class="red_btn" value="отправить" />
	</form>
</div>
</div>
<?/*конец формы обратного звонка*/?>
<? $APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR . "/include/footer/script.php")); ?>
<? } ?>
</body>
</html>