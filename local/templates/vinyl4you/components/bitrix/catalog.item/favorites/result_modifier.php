<?

if (Bitrix\Main\Loader::includeModule('project.core')) {
    $itemResize = function(&$item) {
        if ($item['ID']) {
            $item['SRC'] = Project\Core\Image::catalog($item['ID'], 200, 200);
        }
    };
    $itemResize($arResult['ITEM']['PREVIEW_PICTURE']);
    $itemResize($arResult['ITEM']['PREVIEW_PICTURE_SECOND']);
    $itemResize($arResult['ITEM']['DETAIL_PICTURE']);
    $itemResize($arResult['ITEM']['PRODUCT_PREVIEW']);
    $itemResize($arResult['ITEM']['PRODUCT_PREVIEW_SECOND']);
    foreach ($arResult['ITEM']['MORE_PHOTO'] as $key => &$value) {
        $itemResize($value);
    }
    foreach ($arResult['ITEM']['OFFERS'] as &$offers) {
        $itemResize($offers['PREVIEW_PICTURE']);
        $itemResize($offers['PREVIEW_PICTURE_SECOND']);
        $itemResize($offers['DETAIL_PICTURE']);
        $itemResize($offers['PRODUCT_PREVIEW']);
        $itemResize($offers['PRODUCT_PREVIEW_SECOND']);
        foreach ($offers['MORE_PHOTO'] as $key => &$value) {
            $itemResize($value);
        }
    }
    foreach ($arResult['ITEM']['JS_OFFERS'] as &$offers) {
       $itemResize($offers['PREVIEW_PICTURE']);
        $itemResize($offers['PREVIEW_PICTURE_SECOND']);
        $itemResize($offers['DETAIL_PICTURE']);
        $itemResize($offers['PRODUCT_PREVIEW']);
        $itemResize($offers['PRODUCT_PREVIEW_SECOND']);
        foreach ($offers['MORE_PHOTO'] as $key => &$value) {
            $itemResize($value);
        }
    }
}

$arParams['MESS_BTN_BUY'] = $arParams['MESS_BTN_ADD_TO_BASKET'] = 'Быстрый заказ';

$fixOldPrice = function(&$item, $old) {
    if ($old) {
        $price = &$item['ITEM_PRICES'][0];
        $price['PERCENT'] = round(($old - $price['RATIO_PRICE']) / $old * 100, 0);
        $price['RATIO_BASE_PRICE'] = $price['BASE_PRICE'] = $old;
        $price['PRINT_BASE_PRICE'] = $price['PRINT_RATIO_BASE_PRICE'] = CCurrencyLang::CurrencyFormat($price['RATIO_PRICE'], $price['CURRENCY']);
    }
};

$haveOffers = !empty($arResult['ITEM']['OFFERS']);
if ($haveOffers) {
    $price = $index = 0;
    foreach ($arResult['ITEM']['OFFERS'] as $key => &$arItem) {
        $priceItem = $arItem['ITEM_PRICES'][0]['BASE_PRICE'];
        if (empty($price) or $price > $priceItem) {
            $price = $priceItem;
            $arResult['ITEM']['OFFERS_SELECTED'] = $key;
        }
        $old = $arItem['PROPERTIES']['OLD_PRICE']['VALUE'];
        $fixOldPrice($arItem, $old);
        $fixOldPrice($arResult['ITEM']['JS_OFFERS'][$key], $old);
    }
}