<?

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();
/** @var CBitrixComponentTemplate $this */
/** @var array $arParams */
/** @var array $arResult */
/** @global CDatabase $DB */
$this->setFrameMode(true);

$listId = array_keys($arResult['ITEMS']);
    if($listId) {
    $APPLICATION->IncludeComponent('project:product.list', 'top', array(
        'HEADER' => 'Рекомендумые товары',
        'FILTER' => array(
            'ID' => $listId
        )
    ));
}
