<?

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
    die();

/**
 * @var CBitrixComponentTemplate $this
 * @var CatalogElementComponent $component
 */
$component = $this->getComponent();
$arParams = $component->applyTemplateModifications();

$fixOldPrice = function(&$item, $old) {
    if ($old) {
        $price = &$item['ITEM_PRICES'][0];
        $price['PERCENT'] = round(($old - $price['RATIO_PRICE'])/$old*100, 0);
        $price['RATIO_BASE_PRICE'] = $price['BASE_PRICE'] = $old;
        $price['PRINT_BASE_PRICE'] = $price['PRINT_RATIO_BASE_PRICE'] = CCurrencyLang::CurrencyFormat($price['RATIO_PRICE'], $price['CURRENCY']);
    }
};

$haveOffers = !empty($arResult['OFFERS']);
if ($haveOffers) {
    $price = $index = 0;
    foreach ($arResult['OFFERS'] as $key => $arItem) {
        $priceItem = $arItem['ITEM_PRICES'][0]['BASE_PRICE'];
        if(empty($price) or $price>$priceItem) {
            $price = $priceItem;
            $arResult['OFFERS_SELECTED'] = $key;
        }
        $old = $arItem['PROPERTIES']['OLD_PRICE']['VALUE'];
        $fixOldPrice($arItem, $old);
        $fixOldPrice($arResult['JS_OFFERS'][$key], $old);
    }
} else {

}

//if(Bitrix\Main\Loader::includeModule('project.core')) {
//    foreach ($arResult['MORE_PHOTO'] as $key => $value) {
//        $arResult['MORE_PHOTO_MEDIUM'][$key]['SRC'] = Project\Core\Image::resize($value['ID'], 280, 280);
//        $arResult['MORE_PHOTO_SMALL'][$key]['SRC'] = Project\Core\Image::resize($value['ID'], 60, 60);
//    }
//    foreach ($arResult['OFFERS'] as $keyOffers => $arOffers) {
//        foreach ($arOffers['MORE_PHOTO'] as $key => $value) {
//            $arResult['OFFERS'][$keyOffers]['MORE_PHOTO_MEDIUM'][$key]['SRC'] = Project\Core\Image::resize($value['ID'], 280, 280);
//            $arResult['OFFERS'][$keyOffers]['MORE_PHOTO_SMALL'][$key]['SRC'] = Project\Core\Image::resize($value['ID'], 60, 60);
//        }
//    }
//}



if(Bitrix\Main\Loader::includeModule('project.core')) {
    $itemResize = function(&$item) {
        if ($item['ID']) {
            $item['SRC'] = Project\Core\Image::catalog($item['ID'], 1200, 1200);
        }
    };
    foreach ($arResult['MORE_PHOTO'] as &$value) {
        $itemResize($value);
    }
}