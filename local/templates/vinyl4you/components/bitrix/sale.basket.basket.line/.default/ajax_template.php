<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
    die();

$this->IncludeLangFile('template.php');

$cartId = $arParams['cartId'];

require(realpath(dirname(__FILE__)) . '/top_template.php');

if ($arParams["SHOW_PRODUCTS"] == "Y" && $arResult['NUM_PRODUCTS'] > 0) {
    ?>
    <div id="shopping_cart_mini" class="hidden-phone hidden-tablet">
        <div class="inner-wrapper">
            <table class="table_style">
                <tbody>
                    <?
                    foreach ($arResult["CATEGORIES"] as $category => $items) {
                        if (empty($items))
                            continue;
                        ?>
                        <? foreach ($items as $v) { ?>
                            <tr class="item">
                                <td class="t_right v_align_top"><?= $v["QUANTITY"] ?>&nbsp;x&nbsp;</td>
                                <td class="v_align_top">
                                    <? if ($v["DETAIL_PAGE_URL"]): ?>
                                        <a href="<?= $v["DETAIL_PAGE_URL"] ?>"><?= $v["NAME"] ?></a>
                                    <? else: ?>
                                        <?= $v["NAME"] ?>
                                    <? endif ?>
                                </td>
                            </tr>
                        <? } ?>
                    <? } ?>
                </tbody>
            </table>
            <div class="wrapper">
                <a class="button" title="Просмотр корзины" href="<?= $arParams['PATH_TO_BASKET'] ?>">В корзину</a>
                <a class="button" title="Оформление заказа" href="<?= $arParams['PATH_TO_ORDER'] ?>">Оформить</a>
            </div>
        </div>
    </div>
    <script>
        BX.ready(function () {
    <?= $cartId ?>.fixCart();
        });
    </script>
    <?
}