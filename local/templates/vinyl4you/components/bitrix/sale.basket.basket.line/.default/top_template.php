<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
    die();
/**
 * @global array $arParams
 * @global CUser $USER
 * @global CMain $APPLICATION
 * @global string $cartId
 */
?>

<a href="<?= $arParams['PATH_TO_BASKET'] ?>" class="shopping_cart"  title="Просмотреть корзину">
    <span id="cart-total">
        <?= $arResult['NUM_PRODUCTS'] ?>
    </span>
</a>