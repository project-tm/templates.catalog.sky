<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
    die();

/**
 * @var array $arParams
 * @var array $templateData
 * @var string $templateFolder
 * @var CatalogSectionComponent $component
 */
global $APPLICATION;

$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR . "/include/catalog/section.php"), false);

if (!empty($templateData['TEMPLATE_LIBRARY'])) {
    $loadCurrency = false;
    if (!empty($templateData['CURRENCIES'])) {
        $loadCurrency = \Bitrix\Main\Loader::includeModule('currency');
    }

    CJSCore::Init($templateData['TEMPLATE_LIBRARY']);

    if ($loadCurrency) {
        ?>
        <script>
            BX.Currency.setCurrencies(<?= $templateData['CURRENCIES'] ?>);
        </script>
        <?
    }
}

$APPLICATION->IncludeComponent(
        "project:favorites.ajax", "", array(
    'TYPE' => 'SHOP',
    'SELECT' => '.add-to-links-section .wishlist',
    'ELEMENTS' => $arResult['ITEM_LIST']
        )
);
