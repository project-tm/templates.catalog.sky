<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<?
$bDefaultColumns = $arResult["GRID"]["DEFAULT_COLUMNS"];
$colspan = ($bDefaultColumns) ? count($arResult["GRID"]["HEADERS"]) : count($arResult["GRID"]["HEADERS"]) - 1;
$bPropsColumn = false;
$bUseDiscount = false;
$bPriceType = false;
$bShowNameWithPicture = ($bDefaultColumns) ? true : false; // flat to show name and picture column in one column
?>
<div class="bx_ordercart">
    <div class="field">
        <label for="ORDER_DESCRIPTION">
            <?= GetMessage("SOA_TEMPL_SUM_COMMENTS") ?>
        </label>
        <div class="input-box">
            <div class="bx_block r3x1">
                <textarea name="ORDER_DESCRIPTION" id="ORDER_DESCRIPTION" style="max-width:100%;min-height:120px"><?= $arResult["USER_VALS"]["ORDER_DESCRIPTION"] ?></textarea>
            </div>
        </div>
        <script>
//            (window.top.isBxSale ? window.BX : window.BX).saleOrderAjax.addPropertyDesc({'id': '7', 'attributes': {'type': 'TEXTAREA', 'valueSource': 'default'}});
        </script>
        <div style="clear:both"></div>
    </div>
</div>
