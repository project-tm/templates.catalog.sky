<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();
CJSCore::Init(array("fx", 'ajax', 'popup'));
$curPage = $APPLICATION->GetCurPage(false);
$isMain = $curPage == '/';

use Bitrix\Main\Localization\Loc,
    Bitrix\Main\Page\Asset;

Loc::loadMessages(__FILE__);
?>
<!DOCTYPE html>
<html xml:lang="<?= LANGUAGE_ID ?>" lang="<?= LANGUAGE_ID ?>">
    <head>
        <meta http-equiv="Content-Language" content="<?= LANGUAGE_ID ?>">
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <link rel="icon" href="/favicon.ico" type="image/x-icon">
        <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
        <? $APPLICATION->ShowHead(); ?>
        <?
        if (empty($_GET['test'])) {
            $APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH . "/assets/bootstrap.css");
            $APPLICATION->SetAdditionalCSS("/bitrix/css/main/font-awesome.css");
//            Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . '/assets/components/select2/dist/css/select2.min.css');
            Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . '/assets/styles/camera.css');
            Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . '/assets/styles/style.css');
            Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . '/custom.css');
            Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . '/assets/styles/jquery.ui.all.min.css');

            Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . '/assets/themes/red/style.css');

            Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . '/assets/scripts/jquery.min.js');

            Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . '/assets/styles/jquery.fancybox.css');
            Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . '/assets/scripts/jquery.fancybox.pack.js');
//            Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . '/assets/components/fancybox/dist/jquery.fancybox.min.css');
//            Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . '/assets/components/fancybox/dist/jquery.fancybox.min.js');

//            Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . '/assets/components/select2/dist/js/select2.min.js');

            Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . '/assets/components/notify.min.js');
            Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . '/assets/components/jquery.cookie/jquery.cookie.js');
            Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . '/assets/components/jquery-validation/dist/jquery.validate.min.js');

            Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . '/assets/scripts/forall.js');
            Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . '/assets/scripts/camera.min.js');
            Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . '/assets/scripts/main.js');
            Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . '/assets/scripts/jquery.matchHeight.js');
            Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . '/js/script.js');

            Asset::getInstance()->addJs('//vk.com/js/api/openapi.js');
            Asset::getInstance()->addString('<script type="text/javascript">var zoomloaderPath = "' . SITE_TEMPLATE_PATH . '/images/loader.gif' . '";</script>');
            Asset::getInstance()->addString('<script type="text/javascript">(window.Image ? (new Image()) : document.createElement("img")).src = location.protocol + "//vk.com/rtrg?r=rJ9ecmuINmxKbJFGVkVnnjzW/ch9PpsomXPdQ2DbdFM66Mq2ZO80Qp4BuOmfaPi/ER2mGj8NVJ1Cu5VVuN4AzXkoKqgyl9xuGW3p0aG6eUXq0umNrFlGQWf8cxsHSQP8uIn6ou9rRukinrOqd7eUdUqo69Vnc*HXtqWWRUODEnI-&pixel_id=1000046120";</script>');
        }
//	$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH."/colors.css", true);
        ?>
        <title><? $APPLICATION->ShowTitle() ?></title>
        <script>
				$(function() {
					$('.product-item').matchHeight();
				});
            var jsIsAuth = <?= (int) CUser::IsAuthorized() ?>;
            var jsPopupOpen = <?= (int) defined('TEMPLATES_IS_POPUP') ?>;
        </script>
    </head>
    <body class="<?= $APPLICATION->ShowProperty("bodyClass") ?> order-wrap">
        <? $APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR . "/include/header/script.php"), false); ?>
        <? if(!defined('TEMPLATES_IS_POPUP')) { ?>
        <div id="panel"><? $APPLICATION->ShowPanel(); ?></div>
        <div id="header_bar" class="header_bar <?= $APPLICATION->ShowProperty("headerBarClass") ?>">
            <div class="container bg-hdr">
                <div class="d-flex">
                    <div class="logo">
                        <? $APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR . "/include/header/logo.php"), false); ?>
                    </div>
                    <!-- Форма поиска -->
                    <? $APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR . "/include/header/search.php"), false); ?>
                    <!-- /Форма поиска -->
                    <div class="clear"></div>
                </div>
            </div>
            <div class="shdw"></div>
        </div>
        <div id="bodyWrapper" class="main_page">
            <!-- Сообщение для инвалидов -->
            <noscript>
            <div class="noscript">
                <div class="noscript-inner">
                    <p><strong>Мы заметили что у Вас выключен JavaScript.</strong></p>
                    <p>Необходимо включить его для корректной работы сайта.</p>
                </div>
            </div>
            </noscript>
            <!-- /Сообщение для инвалидов -->
            <div class='vlad'>
                <div id="header">
                    <div class="container">
                        <div class="vlad-wrap">
                            <div class="logo">
                                <? $APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR . "/include/header/logo.php")); ?>
                            </div>
                            <? $APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR . "/include/header/contacts.php")); ?>
<div class="callback_btn_wrap"><a href="#" class="callback_btn">Обратный Звонок</a></div>
                            <?
                            $APPLICATION->IncludeComponent(
                                    "bitrix:sale.basket.basket.line", ".default", array(
                                "PATH_TO_BASKET" => SITE_DIR . "personal/cart/",
                                "PATH_TO_PERSONAL" => SITE_DIR . "personal/",
                                "SHOW_PERSONAL_LINK" => "N",
                                "SHOW_NUM_PRODUCTS" => "Y",
                                "SHOW_TOTAL_PRICE" => "N",
                                "SHOW_PRODUCTS" => "Y",
                                "POSITION_FIXED" => "N",
                                "SHOW_AUTHOR" => "N",
                                "PATH_TO_REGISTER" => SITE_DIR . "login/",
                                "PATH_TO_PROFILE" => SITE_DIR . "personal/",
                                "COMPONENT_TEMPLATE" => ".default",
                                "PATH_TO_ORDER" => SITE_DIR . "personal/order/make/",
                                "SHOW_EMPTY_VALUES" => "Y",
                                "SHOW_DELAY" => "N",
                                "SHOW_NOTAVAIL" => "N",
                                "SHOW_SUBSCRIBE" => "N",
                                "SHOW_IMAGE" => "N",
                                "SHOW_PRICE" => "Y",
                                "SHOW_SUMMARY" => "Y",
                                "HIDE_ON_BASKET_PAGES" => "Y"
                                    ), false
                            );
                            ?>
<? $APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR . "/include/header/search.php"), false); ?>
                            <!-- /Форма поиска -->
                            <div class="clear"></div>
                        </div>
                        <?
                        $APPLICATION->IncludeComponent("bitrix:menu", "menu.nav", Array(
                            "ROOT_MENU_TYPE" => "top", // Тип меню для первого уровня
                            "MAX_LEVEL" => "1", // Уровень вложенности меню
                            "CHILD_MENU_TYPE" => "", // Тип меню для остальных уровней
                            "USE_EXT" => "N", // Подключать файлы с именами вида .тип_меню.menu_ext.php
                            "DELAY" => "N", // Откладывать выполнение шаблона меню
                            "ALLOW_MULTI_SELECT" => "N", // Разрешить несколько активных пунктов одновременно
                            "MENU_CACHE_TYPE" => "N", // Тип кеширования
                            "MENU_CACHE_TIME" => "3600", // Время кеширования (сек.)
                            "MENU_CACHE_USE_GROUPS" => "Y", // Учитывать права доступа
                            "MENU_CACHE_GET_VARS" => "", // Значимые переменные запроса
                            "COMPONENT_TEMPLATE" => "menu.nav"
                                ), false
                        );
                        ?>
                    </div>
                </div>
            </div>
        </div>

        <? if ($isMain) { ?>
            <? $APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR . "/include/main/header.php")); ?>
        <? } ?>
<? $APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR . "/include/header/promo.php")); ?>

        <div class="container contentText">
            <div id="columnLeft" class="product_info_page_left four columns alpha">
                <?
                $APPLICATION->IncludeComponent("bitrix:menu", "menu.catalog", Array(
                    "ROOT_MENU_TYPE" => "left", // Тип меню для первого уровня
                    "MAX_LEVEL" => "3", // Уровень вложенности меню
                    "CHILD_MENU_TYPE" => "", // Тип меню для остальных уровней
                    "USE_EXT" => "Y", // Подключать файлы с именами вида .тип_меню.menu_ext.php
                    "DELAY" => "N", // Откладывать выполнение шаблона меню
                    "ALLOW_MULTI_SELECT" => "N", // Разрешить несколько активных пунктов одновременно
                    "MENU_CACHE_TYPE" => "N", // Тип кеширования
                    "MENU_CACHE_TIME" => "3600", // Время кеширования (сек.)
                    "MENU_CACHE_USE_GROUPS" => "Y", // Учитывать права доступа
                    "MENU_CACHE_GET_VARS" => "", // Значимые переменные запроса
                    "COMPONENT_TEMPLATE" => "menu.catalog"
                        ), false
                );
                ?>
                <? if (1) { ?>
                    <? $APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR . "/include/left.menu/compare.php")); ?>
                    <? $APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR . "/include/left.menu/viewed.php")); ?>
                <? } ?>
<? $APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR . "/include/left.menu/vk.php")); ?>
            </div>

            <div class="twelve columns omega main_part_wrapper">
                <? if (!$isMain) { ?>
                    <?
                    $APPLICATION->IncludeComponent(
                            "bitrix:breadcrumb", "breadcrumb.navigation", Array(
                        "PATH" => "",
                        "SITE_ID" => "s1",
                        "START_FROM" => "0"
                            )
                    );
                    ?>
                <? } ?>
                <? if (defined('TEMPLATES_IS_PERSONAL')) { ?>
                    <h1 class="heading_title"><?= $APPLICATION->ShowTitle(false); ?></h1>
<? } else if (!defined('TEMPLATES_IS_MAIN') and ! defined('TEMPLATES_IS_CATALOG')) { ?>
                    <div class="inner">
                        <div id="content">
                            <div class="box">
                                <div class="box-content htmlDataBlock">
                                    <h1 class="heading_title"><?= $APPLICATION->ShowTitle(false); ?></h1>
<? } ?>
<? } ?>