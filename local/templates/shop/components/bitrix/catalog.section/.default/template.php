<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
    die();

use \Bitrix\Main\Localization\Loc;

/**
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $arResult
 * @var CatalogSectionComponent $component
 * @var CBitrixComponentTemplate $this
 * @var string $templateName
 * @var string $componentPath
 */
$this->setFrameMode(true);

if (!empty($arResult['NAV_RESULT'])) {
    $navParams = array(
        'NavPageCount' => $arResult['NAV_RESULT']->NavPageCount,
        'NavPageNomer' => $arResult['NAV_RESULT']->NavPageNomer,
        'NavNum' => $arResult['NAV_RESULT']->NavNum
    );
} else {
    $navParams = array(
        'NavPageCount' => 1,
        'NavPageNomer' => 1,
        'NavNum' => $this->randString()
    );
}

if (!empty($arResult['ITEMS']) && !empty($arResult['ITEM_ROWS'])) {
    ?>
    <div class="wrap-games wrap-games--section row">
        <?
        foreach ($arResult['ITEMS'] as $arItem) {
            //print_r($arItem);
            ?>
            <div class="btrp-col col-lg-3 col-md-4 col-sm-6 col-xs-12 games-section-item">
                <div class="games">
                    <a class="img" href="<?= $arItem['DETAIL_PAGE_URL'] ?>" style="background: url(<? if ($arItem['PREVIEW_PICTURE']['SRC'] != '') { ?><?= $arItem['PREVIEW_PICTURE']['SRC'] ?><? } else { ?>/local/templates/shop/components/bitrix/catalog.element/.default/images/no_photo.png<? } ?>);"></a>
                    <div class="info-game">
                        <a class="name" href="<?= $arItem['DETAIL_PAGE_URL'] ?>"><?= $arItem['NAME'] ?>
                        </a>
                        <div class="platforms">
                            <?
                            $ar_console = array();
                            foreach ($arItem['OFFERS'] as $offer) {
                                if (!in_array($offer['PROPERTIES']['M_PLATFORM']['VALUE'], $ar_console)) {
                                    $ar_console[] = $offer['PROPERTIES']['M_PLATFORM']['VALUE'];
                                    ?>
                                    <div class="console"><?= $offer['PROPERTIES']['M_PLATFORM']['VALUE'] ?></div>
                                    <?
                                }
                            }
                            ?>
                        </div>
                    </div>
                    <div class="<? if (count($arItem['OFFERS']) > 1): ?>wrap-absolute<? endif; ?>">
                        <?
                        foreach ($arItem['OFFERS'] as $offer) {
                            if ($offer['PROPERTIES']['M_WEBVERSION']['VALUE'] != TEMPLATES_SKY_WEBVERSION) {
                                ?>
                                <div class="prise">
                                    <div class="current-platform"><span><?= $offer['PROPERTIES']['M_PLATFORM']['VALUE'] ?></span></div>
                                    <div class="money">
                                        <?= $offer['ITEM_PRICES'][0]['BASE_PRICE'] ?><span class="small-rub">р.</span>
                                    </div>
                                    <a class="buy list-sky-basket" data-id="<?=$arItem['ID'] ?>" data-offers="<?=$arItem['ID'] ?>-<?=$offer['PROPERTIES']['M_PLATFORM']['VALUE_ENUM_ID'] ?>" href="<?= $offer['~BUY_URL'] ?>"></a>
                                </div>
                                <?
                            }
                        }
                        ?>
                    </div>
                </div>
            </div>
            <?
        }
        ?>
    </div>
    <? include(__DIR__ .'/template.v2.php') ?>
    <div data-pagination-num="<?= $navParams['NavNum'] ?>">
        <!-- pagination-container -->
        <?= $arResult['NAV_STRING'] ?>
        <!-- pagination-container -->
    </div>
    <?
}
