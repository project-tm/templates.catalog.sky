<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

/**
 * @var CBitrixComponentTemplate $this
 * @var CatalogSectionComponent $component
 */

//print_r($arResult);

$component = $this->getComponent();
$arParams = $component->applyTemplateModifications();