<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

if (isset($_REQUEST['typeCatalog'])) {
    foreach ($_REQUEST as $key => $value) {
        if($key{0}=='?') {
            unset($_REQUEST[$key]);
            unset($_GET[$key]);
        }
    }

    switch ($_REQUEST['typeCatalog']) {
        case 'new':
            $APPLICATION->SetTitle("Новинки");
            break;
        case 'specialoffer':
            $APPLICATION->SetTitle("Спецпредложения");
            break;
        case 'sale':
            $APPLICATION->SetTitle("Самое продаваемое");
            break;
        case 'gift':
            $APPLICATION->SetTitle("Идеи подарков");
            break;

        default:
            break;
    }
    $APPLICATION->AddChainItem($APPLICATION->GetTitle());
    switch ($_REQUEST['typeCatalog']) {
        case 'new':
        case 'specialoffer':
        case 'sale':
        case 'gift':
            define('TEMPLATES_CATALOG_TYPE', $_REQUEST['typeCatalog']);
            $arResult['VARIABLES']['SECTION_ID'] = '';
            $arResult['VARIABLES']['SECTION_CODE'] = '';
            $arParams['SHOW_ALL_WO_SECTION'] = 'Y';
            $GLOBALS["arrFilter"] = array(
                "!PROPERTY_" . strtoupper(TEMPLATES_CATALOG_TYPE) => false
            );
            include __DIR__ . '/section.php';
            return;

        default:
            break;
    }
}

//$this->addExternalCss("/bitrix/css/main/bootstrap.css");
?>
<h2><a href="/sale/">Самое продаваемое</a></h2>
<?
$APPLICATION->IncludeComponent("custom:top.product", ".default", array('TYPE' => 'SALE'));
?>
<h2><a href="/specialoffer/">Спецпредложения</a></h2>
<?
$APPLICATION->IncludeComponent("custom:top.product", ".default", array('TYPE' => 'SPECIALOFFER'));
?>
<h2><a href="/gift/">Идеи подарков</a></h2>
<?
$APPLICATION->IncludeComponent("custom:top.product", ".default", array('TYPE' => 'GIFT'));
?>
<h2><a href="/new/">Новинки</a></h2>
<?
$APPLICATION->IncludeComponent("custom:top.product", ".default", array('TYPE' => 'NEWPRODUCT'));
?>

