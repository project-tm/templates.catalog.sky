<div class="sort_filter">
    <?
    $basePrice = CCatalogGroup::GetBaseGroup();
    $priceSort = "CATALOG_PRICE_" . $basePrice["ID"];
    $arAvailableSort = array(
        "NAME" => array("NAME", "asc"),
        "PRICE" => array($priceSort, "asc")
    );
    $arParams["ELEMENT_SORT_ORDER"] = $arAvailableSort[$arParams["ELEMENT_SORT_FIELD"]][1];
    if ($arParams["SHOW_QUANTITY_SORT"] == "Y") {
        $arAvailableSort["QUANTITY"] = array("CATALOG_QUANTITY", "desc");
    }
    $sort = $arParams["ELEMENT_SORT_FIELD"];
    if ((array_key_exists("sort", $_REQUEST) && array_key_exists(ToUpper($_REQUEST["sort"]), $arAvailableSort)) || $arParams["ELEMENT_SORT_FIELD"]) {
        if ($_REQUEST["sort"]) {
            $sort = ToUpper($_REQUEST["sort"]);
        } else {
            $sort = ToUpper($arParams["ELEMENT_SORT_FIELD"]);
        }
    }
    $sort_order = $arAvailableSort[$sort][1];
    if ((array_key_exists("order", $_REQUEST) && in_array(ToLower($_REQUEST["order"]), Array("asc", "desc")) ) || (array_key_exists("order", $_REQUEST) && in_array(ToLower($_REQUEST["order"]), Array("asc", "desc")) ) || $arParams["ELEMENT_SORT_ORDER"]) {
        if ($_REQUEST["order"]) {
            $sort_order = $_REQUEST["order"];
        } else {
            $sort_order = ToLower($arParams["ELEMENT_SORT_ORDER"]);
        }
    }
    foreach ($arAvailableSort as $key => $val) {
        $newSort = $sort_order == 'desc' ? 'asc' : 'desc';
        ?>
        <a rel="nofollow" href="<?= $APPLICATION->GetCurPageParam('sort=' . $key . '&order=' . $newSort, array('sort', 'order', 'mode', 'clear_cache', 'typeCatalog', 'bitrix_include_areas')) ?>" class="button_middle <?= $sort == $key ? 'current' : '' ?> <?= ToLower($sort_order) ?> <?= $key ?>" rel="nofollow">
            <i></i><span><?= getMessage('SECT_SORT_' . $key) ?></span>
        </a>
    <? } ?>
</div>
<?
$arParams["ELEMENT_SORT_FIELD"] = $arAvailableSort[$sort][0];;
$arParams["ELEMENT_SORT_ORDER"] = $sort_order;
?>