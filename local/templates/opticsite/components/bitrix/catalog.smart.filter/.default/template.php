<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

$templateData = array(
    'TEMPLATE_THEME' => $this->GetFolder() . '/themes/' . $arParams['TEMPLATE_THEME'] . '/colors.css',
    'TEMPLATE_CLASS' => 'bx-' . $arParams['TEMPLATE_THEME']
);

//if (isset($templateData['TEMPLATE_THEME'])) {
//    $this->addExternalCss($templateData['TEMPLATE_THEME']);
//}
$this->addExternalCss("/bitrix/css/main/bootstrap.css");
//$this->addExternalCss("/bitrix/css/main/font-awesome.css");
?>
<div class="bx-filter custom_filter <?= $templateData["TEMPLATE_CLASS"] ?> <? if ($arParams["FILTER_VIEW_MODE"] == "HORIZONTAL") echo "bx-filter-horizontal" ?>">
    <div class="bx-filter-section container-fluid">
        <form class="form_work_st" name="<? echo $arResult["FILTER_NAME"] . "_form" ?>" action="<? echo $arResult["FORM_ACTION"] ?>" method="get" class="smartfilter">
            <? foreach ($arResult["HIDDEN"] as $arItem): ?>
                <input type="hidden" name="<? echo $arItem["CONTROL_NAME"] ?>" id="<? echo $arItem["CONTROL_ID"] ?>" value="<? echo $arItem["HTML_VALUE"] ?>" />
            <? endforeach; ?>
            <div class="delimiter">
            <?
            $isView = array();
            $isHeader = true;
            foreach ($arResult["ITEMS"] as $key => $arItem) {
                if (empty($arItem['CODE']) or $arItem['CODE'] == 'sex' or $arItem['CODE'] == 'LENSES_ASTIGMATISM') {
                    continue;
                }
                $isView[$key] = $key;
                if (
                        empty($arItem["VALUES"]) || isset($arItem["PRICE"])
                )
                    continue;
                if (
                        $arItem["DISPLAY_TYPE"] == "A" && (
                        $arItem["VALUES"]["MAX"]["VALUE"] - $arItem["VALUES"]["MIN"]["VALUE"] <= 0
                        )
                )
                    continue;
                include(__DIR__ . '/item.php');
            }
            ?>
                <div class="col_right">
                    <div class="result-count">
                    <? if (isset($arResult['ELEMENT_COUNT_ALL'])) {
                        ?>
                        <span>Подобрано</span><br/>
                        <div class="result-count__count" id="countitems">
                            <?=intval($arResult["ELEMENT_COUNT_ALL"])?>
                        </div>
                        <span>результатов</span><br/>
                        <?
                    } else {
                        ?>
                        <span>Показаны</span><br/>
                        <span>все</span><br/>
                        <span>результаты</span><br/>
                        <?
                    }
                    ?>
                    </div>
                </div>
            </div>
            <div class="go_to_bottom">
            <?
            $isHeader = false;
            foreach ($arResult["ITEMS"] as $key => $arItem) {
                if (empty($arItem['CODE']) or isset($isView[$key])) {
                    continue;
                }
                if (
                        empty($arItem["VALUES"]) || isset($arItem["PRICE"])
                )
                    continue;
                if (
                        $arItem["DISPLAY_TYPE"] == "A" && (
                        $arItem["VALUES"]["MAX"]["VALUE"] - $arItem["VALUES"]["MIN"]["VALUE"] <= 0
                        )
                )
                    continue;
                include(__DIR__ . '/item.php');
            }
            ?>
            </div>
            <?
            foreach ($arResult["ITEMS"] as $key => $arItem) {//prices
                $key = $arItem["ENCODED_ID"];
                if (isset($arItem["PRICE"])):
                    if ($arItem["VALUES"]["MAX"]["VALUE"] - $arItem["VALUES"]["MIN"]["VALUE"] <= 0)
                        continue;

                    $step_num = 4;
                    $step = ($arItem["VALUES"]["MAX"]["VALUE"] - $arItem["VALUES"]["MIN"]["VALUE"]) / $step_num;
                    $prices = array();
                    if (Bitrix\Main\Loader::includeModule("currency")) {
                        for ($i = 0; $i < $step_num; $i++) {
                            $prices[$i] = CCurrencyLang::CurrencyFormat($arItem["VALUES"]["MIN"]["VALUE"] + $step * $i, $arItem["VALUES"]["MIN"]["CURRENCY"], false);
                        }
                        $prices[$step_num] = CCurrencyLang::CurrencyFormat($arItem["VALUES"]["MAX"]["VALUE"], $arItem["VALUES"]["MAX"]["CURRENCY"], false);
                    } else {
                        $precision = $arItem["DECIMALS"] ? $arItem["DECIMALS"] : 0;
                        for ($i = 0; $i < $step_num; $i++) {
                            $prices[$i] = number_format($arItem["VALUES"]["MIN"]["VALUE"] + $step * $i, $precision, ".", "");
                        }
                        $prices[$step_num] = number_format($arItem["VALUES"]["MAX"]["VALUE"], $precision, ".", "");
                    }
                    ?>
                    <div class="filter_h <? if ($arParams["FILTER_VIEW_MODE"] == "HORIZONTAL"): ?> <? else: ?> <? endif ?> bx-filter-parameters-box bx-active">
                        <span class="bx-filter-container-modef"></span>
                        <div class="bx-filter-parameters-box-title">Цена:</div>
                        <div class="bx-filter-block" data-role="bx_filter_block">
                            <div class="row bx-filter-parameters-box-container">
                                <div class="hidden">
                                    <input
                                        class="min-price"
                                        type="text"
                                        name="<? echo $arItem["VALUES"]["MIN"]["CONTROL_NAME"] ?>"
                                        id="<? echo $arItem["VALUES"]["MIN"]["CONTROL_ID"] ?>"
                                        value="<? echo $arItem["VALUES"]["MIN"]["HTML_VALUE"] ?>"
                                        size="5"
                                        onkeyup="smartFilter.keyup(this)"
                                        />
                                </div>
                                <div class="hidden">
                                    <input
                                        class="max-price"
                                        type="text"
                                        name="<? echo $arItem["VALUES"]["MAX"]["CONTROL_NAME"] ?>"
                                        id="<? echo $arItem["VALUES"]["MAX"]["CONTROL_ID"] ?>"
                                        value="<? echo $arItem["VALUES"]["MAX"]["HTML_VALUE"] ?>"
                                        size="5"
                                        onkeyup="smartFilter.keyup(this)"
                                        />
                                </div>

                                <div class="col-xs-10 col-xs-offset-1 bx-ui-slider-track-container">
                                    <div class="bx-ui-slider-track" id="drag_track_<?= $key ?>">
                                        <div class="bx-ui-slider-pricebar-vd" style="left: 0;right: 0;" id="colorUnavailableActive_<?=$key?>"></div>
                                        <div class="bx-ui-slider-pricebar-vn" style="left: 0;right: 0;" id="colorAvailableInactive_<?=$key?>"></div>
                                        <div class="bx-ui-slider-pricebar-v"  style="left: 0;right: 0;" id="colorAvailableActive_<?=$key?>">
                                            <span id="both_<?=$key?>_count" class="bothcount">
                                                <span id="left_slider_<?=$key?>_bothcount" class="leftcount"><?echo number_format(intval($arItem["VALUES"]["MIN"]["HTML_VALUE"]) ? $arItem["VALUES"]["MIN"]["HTML_VALUE"] : $arItem["VALUES"]["MIN"]["VALUE"], 0, '', ' ')?></span> — <span id="right_slider_<?=$key?>_bothcount" class="rightcount"><?echo number_format(intval($arItem["VALUES"]["MAX"]["HTML_VALUE"]) ? $arItem["VALUES"]["MAX"]["HTML_VALUE"] : $arItem["VALUES"]["MAX"]["VALUE"], 0, '', ' ')?></span>
                                            </span>
                                        </div>
                                        <div class="bx-ui-slider-range" id="drag_tracker_<?=$key?>"  style="left: 0%; right: 0%;">
                                            <a class="bx-ui-slider-handle left"  style="left:0;" href="javascript:void(0)" id="left_slider_<?=$key?>">
                                                <span id="left_slider_<?=$key?>_count" class="leftcount"><?echo number_format(intval($arItem["VALUES"]["MIN"]["HTML_VALUE"]) ? $arItem["VALUES"]["MIN"]["HTML_VALUE"] : $arItem["VALUES"]["MIN"]["VALUE"], 0, '', ' ')?></span>
                                            </a>
                                            <a class="bx-ui-slider-handle right" style="right:0;" href="javascript:void(0)" id="right_slider_<?=$key?>">
                                                <span id="right_slider_<?=$key?>_count" class="rightcount"><?echo number_format(intval($arItem["VALUES"]["MAX"]["HTML_VALUE"]) ? $arItem["VALUES"]["MAX"]["HTML_VALUE"] : $arItem["VALUES"]["MAX"]["VALUE"], 0, '', ' ')?></span>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?
                    $arJsParams = array(
                        "leftSlider" => 'left_slider_' . $key,
                        "rightSlider" => 'right_slider_' . $key,
                        "leftSliderCount" => 'left_slider_'.$key.'_count',
                        "rightSliderCount" => 'right_slider_'.$key.'_count',
                        "sliderBothcount" => 'both_'.$key.'_count',
                        "leftSliderBothcount" => 'left_slider_'.$key.'_bothcount',
                        "rightSliderBothcount" => 'right_slider_'.$key.'_bothcount',
                        "tracker" => "drag_tracker_" . $key,
                        "trackerWrap" => "drag_track_" . $key,
                        "minInputId" => $arItem["VALUES"]["MIN"]["CONTROL_ID"],
                        "maxInputId" => $arItem["VALUES"]["MAX"]["CONTROL_ID"],
                        "minPrice" => $arItem["VALUES"]["MIN"]["VALUE"],
                        "maxPrice" => $arItem["VALUES"]["MAX"]["VALUE"],
                        "curMinPrice" => $arItem["VALUES"]["MIN"]["HTML_VALUE"],
                        "curMaxPrice" => $arItem["VALUES"]["MAX"]["HTML_VALUE"],
                        "fltMinPrice" => intval($arItem["VALUES"]["MIN"]["FILTERED_VALUE"]) ? $arItem["VALUES"]["MIN"]["FILTERED_VALUE"] : $arItem["VALUES"]["MIN"]["VALUE"],
                        "fltMaxPrice" => intval($arItem["VALUES"]["MAX"]["FILTERED_VALUE"]) ? $arItem["VALUES"]["MAX"]["FILTERED_VALUE"] : $arItem["VALUES"]["MAX"]["VALUE"],
                        "precision" => $precision,
                        "colorUnavailableActive" => 'colorUnavailableActive_' . $key,
                        "colorAvailableActive" => 'colorAvailableActive_' . $key,
                        "colorAvailableInactive" => 'colorAvailableInactive_' . $key,
                    );
                    ?>
                    <script type="text/javascript">
                        BX.ready(function () {
                            window['trackBar<?= $key ?>'] = new BX.Iblock.SmartFilter(<?= CUtil::PhpToJSObject($arJsParams) ?>);
                        });
                    </script>
                    <?
                endif;
            }
            ?>
            <div class="filter__footer">
                <div class="col_left _width_164">Сортировать по:</div>
                <div class="col_left _width_78">Цене <a href="<?= $APPLICATION->GetCurPageParam('sort=price&order=' . ($arParams['SORT'] != 'price' ? 'asc' : $arParams['ORDER']), array('sort', 'order'), false); ?>" class="sort-link sort-link_<?= ($arParams['SORT'] != 'price' ? 'off' : $arParams['ORDER']) ?>"></a></div>
                <div class="col_left _width_220">Названию <a href="<?= $APPLICATION->GetCurPageParam('sort=name&order=' . ($arParams['SORT'] != 'name' ? 'asc' : $arParams['ORDER']), array('sort', 'order'), false); ?>" class="sort-link sort-link_<?= ($arParams['SORT'] != 'name' ? 'off' : $arParams['ORDER']) ?>"></a></div>
                <div class="col_left">Показывать по: <div class="bx-filter-select-container" style="display: inline-block;">
                        <div class="bx-filter-select-block" onclick="smartFilter.showDropDownPopup(this, '_count_elem')" style="display: inline-block; padding: 0 25px 0 10px; background: none;">
                            <div class="bx-filter-select-text" data-role="currentOption" style="display: inline-block;">
                                <?
                                echo $arParams['COUNT_ELEM'];
                                ?>
                            </div>

                            <div class="bx-filter-select-arrow sort-link sort-link_down" style="position: relative;"></div>
                            <input
                                style="display: none"
                                type="radio"
                                name="COUNT_ELEM"
                                id="count_elem"
                                value=""
                                />
                                <? foreach ($arParams['COUNT_ELEMS'] as $val): ?>
                                <input
                                    style="display: none"
                                    type="radio"
                                    name="COUNT_ELEM"
                                    id="COUNT_ELEMS_<?= $val['VALUE'] ?>"
                                    value="<? echo $val['VALUE'] ?>"
                                    <? echo $val['CHECKED'] ? 'checked="checked"' : '' ?>
                                    />
                                <? endforeach ?>
                            <div class="bx-filter-select-popup" data-role="dropdownContent" style="display: none;">
                                <ul>
                                    <li>
                                        <label for="count_elem" class="bx-filter-param-label" data-role="label_count_elem_all" onclick="smartFilter.selectDropDownItem(this, '<?= CUtil::JSEscape("count_elem") ?>">
                                            <?
                                            // echo GetMessage("CT_BCSF_FILTER_ALL");
                                            echo $arParams['COUNT_ELEM'];
                                            ?>
                                        </label>
                                    </li>

                                    <?
                                    foreach ($arParams['COUNT_ELEMS'] as $val):
                                        $class = "";
                                        if ($val['CHECKED'])
                                            $class .= " selected";
                                        ?>
                                        <li>
                                            <label for="COUNT_ELEMS_<?= $val['VALUE'] ?>" class="bx-filter-param-label<?= $class ?>" data-role="label_COUNT_ELEMS_<?= $val['VALUE'] ?>" onclick="smartFilter.selectDropDownItem(this, '<?= CUtil::JSEscape('COUNT_ELEMS_' . $val["VALUE"]) ?>')"><?= $val['VALUE'] ?></label>
                                        </li>
                                    <? endforeach ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col_right">
                    <? $APPLICATION->ShowViewContent('compare.list.filter'); ?>
                </div>
            </div>

            <div class="col-xs-12 bx-filter-button-box hidden">
                <div class="bx-filter-block">
                    <div class="bx-filter-parameters-box-container">
                        <input
                            class="btn btn-themes"
                            type="submit"
                            id="set_filter"
                            name="set_filter"
                            value="<?= GetMessage("CT_BCSF_SET_FILTER") ?>"
                            />
                        <input
                            class="btn btn-link"
                            type="submit"
                            id="del_filter"
                            name="del_filter"
                            value="<?= GetMessage("CT_BCSF_DEL_FILTER") ?>"
                            />
                        <div class="bx-filter-popup-result <? if ($arParams["FILTER_VIEW_MODE"] == "VERTICAL") echo $arParams["POPUP_POSITION"] ?>" id="modef" <? if (!isset($arResult["ELEMENT_COUNT"])) echo 'style="display:none"'; ?> style="display: inline-block;">
                            <? echo GetMessage("CT_BCSF_FILTER_COUNT", array("#ELEMENT_COUNT#" => '<span id="modef_num">' . intval($arResult["ELEMENT_COUNT"]) . '</span>')); ?>
                            <span class="arrow"></span>
                            <br/>
                            <a href="<? echo $arResult["FILTER_URL"] ?>" target=""><? echo GetMessage("CT_BCSF_FILTER_SHOW") ?></a>
                        </div>
                    </div>
                </div>

            </div>
            <div class="clb"></div>
        </form>
    </div>
</div>
<script type="text/javascript">
    var smartFilter = new JCSmartFilter('<? echo CUtil::JSEscape($arResult["FORM_ACTION"]) ?>', '<?= CUtil::JSEscape($arParams["FILTER_VIEW_MODE"]) ?>', <?= CUtil::PhpToJSObject($arResult["JS_FILTER_PARAMS"]) ?>);
</script>